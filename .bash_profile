# $VARIABLE will render before the rest of the command is executed
echo "Logged in as $USER at $(hostname)"

# Path for brew
test -d /usr/local/bin && export PATH="/usr/local/bin:/usr/local/sbin:$PATH"
# Path for Heroku
test -d /usr/local/heroku/ && export PATH="$PATH:/usr/local/heroku/bin"
# Path for Terraform test
test -d $HOME/dev/terraform/bin && export PATH="$PATH:$HOME/dev/terraform/bin"

# Ruby version management
source /usr/local/share/chruby/chruby.sh
# Auto change ruby versions based on .ruby-version
source /usr/local/share/chruby/auto.sh

# Load git completions
git_completion_script=/usr/local/etc/bash_completion.d/git-completion.bash
test -s $git_completion_script && source $git_completion_script

# A more colorful prompt
# \[\e[0m\] resets the color to default color
ColorReset='\[\e[0m\]'
#  \e[0;31m\ sets the color to red
ColorRed='\[\e[0;31m\]'
# \e[0;32m\ sets the color to green
ColorGreen='\[\e[0;32m\]'
# \e[0;36m\ sets the color to green
ColorCyan='\[\e[0;36m\]'


# Git branch in prompt.

parse_git_branch() {

    git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'

}

# PS1 is the variable for the prompt you see everytime you hit enter
git_prompt_script=/usr/local/etc/bash_completion.d/git-prompt.sh
if [ -s $git_prompt_script ]; then
  # if git-prompt is installed, use it (ie. to install it use:
  # `brew install git`)
  GIT_PS1_SHOWUPSTREAM="auto"
  GIT_PS1_SHOWCOLORHINTS="yes"
  source $git_prompt_script
  export PROMPT_COMMAND='__git_ps1 "\u@\h:\W" "\\\$ ";'
else
  # otherwise omit git from the prompt
  export PS1="\n$ColorRed\W$ColorReset :> "
fi

# Colors ls should use for folders, files, symlinks etc, see `man ls` and
# search for LSCOLORS
export LSCOLORS=ExGxFxdxCxDxDxaccxaeex
# Force ls to use colors (G) and use humanized file sizes (h)
alias ls='ls -Gh'

# Force grep to always use the color option and show line numbers
export GREP_OPTIONS='--color=always'

# GOPATH

export GOPATH=$HOME/dev/go 
export GOBIN=$GOPATH/bin
export GO111MODULE=on

# Convenience
alias ll="ls -la"
alias be="bundle exec"
alias bp="code ~/.bash_profile"
alias brc="code ~/.bashrc"
alias sbp=". ~/.bash_profile && echo '.bash_profile reloaded!'"
alias firefox="/Applications/Firefox.app/Contents/MacOS/firefox"

# Journal
alias jo="cd ~/dev/jo && rails s -b 0.0.0.0"
alias cjo="code ~/dev/jo"
alias cdjo="cd ~/dev/jo"

# Archway CRM
alias crm="cd ~/archway/dev/archway_crm"

# MDEmployment
# alias md="cd /Users/Cory/archway/dev/mdemployment"
function md {
  workdir="/Users/Cory/archway/dev/mdemployment"
  cd $workdir;\
  echo "Starting rails server";\
  ttab -Gd $workdir -t 'rails s' bundle exec rails s;\
  echo "Starting docker-compose";\
  ttab -Gd $workdir -t 'docker-compose' docker-compose up;\
  echo "Starting webpack-dev-server";\
  ttab -Gd $workdir -t 'webpack-dev-server' bin/webpack-dev-server;\
  echo "Starting sidekiq after 5 second sleep";\
  sleep 5;\
  ttab -Gd $workdir -t 'sidekiq' bundle exec sidekiq;\
  firefox --new-tab https://archwaydev.atlassian.net/secure/RapidBoard.jspa?rapidView=5
  sleep 1;\
  firefox --new-tab http://localhost:3000;\
  code .
  sleep 5
  fullscreen 'Visual Studio Code'
}

# AllPhysicianCareers
alias apc="cd /Users/Cory/archway/dev/all_physician_careers"

# Redis/Sidkiq
alias sk="bundle exec sidekiq -e development -C config/sidekiq.yml"
alias rds="redis-server"

# Start words
alias words="open ~/Documents/personal/words/words.docx"

# Useful functions

function mkcd {
  last=$(eval "echo \$$#")
  if [ ! -n "$last" ]; then
    echo "Enter a directory name"
  elif [ -d $last ]; then
    echo "\`$last' already exists"
  else
    mkdir $@ && cd $last
  fi
}

function fullscreen() {
  osascript \
  -e "tell application \"$1\"" \
  -e "activate" \
  -e "tell application \"System Events\"" \
  -e "keystroke \"f\" using {control down, command down}" \
  -e "end tell" \
  -e "end tell" 
}




